FROM node:argon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
EXPOSE 8888
# Bundle app source
COPY . /usr/src/app
CMD ["node","ui.js"]
