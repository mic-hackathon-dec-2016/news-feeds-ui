var http = require('http');
var fs=require('fs');
var Client = require('node-rest-client').Client;
var client = new Client();

var express    = require('express');        
var app        = express();                 
var bodyParser = require('body-parser'); 
app.use(bodyParser.urlencoded({ extended: true }));
var port = process.env.PORT || 8888;
var newsFilterServiceHost = process.env.NEWS_FILTER_SERVICE_HOST||'localhost';
var newsFilterServicePort = process.env.NEWS_FILTER_SERVICE_PORT||'9090';

var fillTable=function(newsfeeds,res){
       for(var i=0;i<newsfeeds.length;i++){
       res.write('<tr><td>' + newsfeeds[i].title+'</td>');
       res.write('<td>' + newsfeeds[i].description+'</td>');
       res.write('<td>' + newsfeeds[i].link+'</td></tr>');
       }
      res.write('</table>');
      
}

app.get('/news/:user', function (req, res, next) { 
  res.writeHead(200, {'Content-Type': 'text/html'});
  var jsonOnj; 
  res.write('<!doctype html>\n<html lang="en">\n' +  
  '\n<meta charset="utf-8">\n<title>News Notification App</title>\n' + 
  '<style type="text/css">* {font-family:arial, sans-serif;}</style>\n' + 
  '\n\n<h1>Notification for user:' + req.params.user+'</h1>\n');
  var args={
       headers: { "Accepts": "application/json" }
  }
  client.get('http://'+newsFilterServiceHost+':'+newsFilterServicePort+'/news/dev',args, function (data,response) {
  console.log("Received Data: " + data)
  json=JSON.parse(data);
  res.write('<ul>');  
  json.news.forEach(function(newss){
  console.log("Processing news feeds for " + newss.name);
  res.write('<li>'+newss.name+'</li>\n');
  res.write('<table border="2"><tr><th>Title</th><th>Description</th><th>Link</th></tr>');
  fillTable(newss.newsfeeds,res);
  });
  res.write('</ul>');
  res.end();
  console.log('Processed news feeds successfully');
});
});
app.listen(port);
console.log('App listening on port ' + port);
